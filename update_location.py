
import time
import pymongo
import random


myclient = pymongo.MongoClient("mongodb://localhost:27017/")
mydb = myclient["taxi"]
mycol = mydb["taxi"]
mycol2 = mydb["customer"]
mycol3 = mydb["taxi_customer_reg"]

print("Taxi location are updating...")
def update_location():
    time_counter = 0
    x_list=[]
    y_list=[]
    while(time_counter < 1):
    # get all taxi
     
        taxi_list = mycol.find({})
        for taxi in taxi_list:
           
            x_=random.uniform(0.0001,0.0003)
            y_=random.uniform(0.0001,0.0003)

            x=str(float(taxi["location"]["coordinates"][0])+x_)
            y=str(float(taxi["location"]["coordinates"][1])+y_)
            x_list.append(float(x))
            y_list.append(float(y))
            
            # update location of taxi
            mycol.update_one({"_id": taxi["_id"]}, {"$set": {"location": {'coordinates': [x, y]}}})
        # sleep for 1 minute
       
        time.sleep(60)
        # return taxi list
    return taxi_list

update_location()
        

        
        
