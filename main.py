from flask import Flask

from flask import Flask, request, render_template
import pymongo
import random
import math
from bson import json_util
import json
app = Flask(__name__)



myclient = pymongo.MongoClient("mongodb://localhost:27017/")
mydb = myclient["taxi"]
mycol = mydb["taxi"]
mycol2 = mydb["customer"]
mycol3 = mydb["taxi_customer_reg"]

# API for getting all taxi
@app.route('/Taxi', methods =["GET", "POST"])
def Taxi():
   if request.method == "POST":
      
      taxiName = request.form.get("TaxiName")

      taxiType = request.form.get("TaxiType")
      x= request.form.get("X")
      y= request.form.get("Y")
      # ID = n
   #    print('name', taxiName)
   #    print('type', taxiType)
      #print('Unique id: ', ID)
      myDict = { "name": taxiName, 
               "type": taxiType, "location":{'type': 'point',
                                             'coordinates': [x, y]} }
      mycol.insert_one(myDict)
      print('success')
      
   #  return html page of taxi registration success
   html=""
   html+="<html><body><h1>Taxi Registration Success</h1>"
   html+="<p>Taxi Name: "+taxiName+"</p>"
   html+="<p>Taxi Type: "+taxiType+"</p>"
   html+="<p>X: "+x+"</p>"
   html+="<p>Y: "+y+"</p>"
   html+="</body></html>"
   return html

         #  
   
    # return render_template("index.html")


# API for getting all customer
@app.route('/customer', methods =["GET", "POST"])  
def Customer():
   if request.method == "POST":
      
      customerName = request.form.get("CustomerName")
   
      x= request.form.get("X")
      y= request.form.get("Y")
      # ID = n
      print('name', customerName)

      #print('Unique id: ', ID)
      myDict = { "name": customerName, 
               "location":{'type': 'point','coordinates': [x, y]} }
      mycol2.insert_one(myDict)
      print('success')
       

   #customer registration successfull html page
   html=""
   html+="<html>"
   html+="<head>"
   html+="<title>Customer Registration</title>"
   html+="</head>"
   html+="<body>"
   html+="<h1>Customer Registration Successfull</h1>"
   html+="<p>Customer Name: "+customerName+"</p>"
   html+="<p>Customer Location: "+x+","+y+"</p>"
   html+="</body>"
   html+="</html>"
   return html
   


# API for finding nearest taxiName from customer location
@app.route('/find_taxis', methods =["GET", "POST"])  
def find_taxis():
   # print data of mycol
   # print request form
   # return("request.form", request.form)
   
   name_=""

   # print("taxi",mycol.find())


   if request.method == "POST":
      x_= request.form.get("X")
      y_= request.form.get("Y")

      aa=mycol.find()
      # details={}
      dis=99999
      for l in aa:
            # finx X cordinate
         name=l['name']
         x = l["location"]["coordinates"][0]
         y = l["location"]["coordinates"][1]
         d=math.sqrt((float(x)-float(x_))**2+(float(y_)-float(y))**2)
         # if d less than dis then update dis
         if d<dis:
            dis=d
            name_=l
      print("name",name_)
      # return  json
      # details['name']=name_['name']
      # details['type']=name_['type']
      # details['x']=name_['location']['coordinates'][0]
      # details['y']=name_['location']['coordinates'][1]

      # return html page
      html=""
      html+="<html><body>"
      html+="<h1>Nearest Taxi</h1>"
      html+="<p>Name: "+name_['name']+"</p>"
      html+="<p>Type: "+name_['type']+"</p>"
      html+="<p>X: "+str(name_['location']['coordinates'][0])+"</p>"
      html+="<p>Y: "+str(name_['location']['coordinates'][1])+"</p>"
      html+="</body></html>"
      return html

      
      # return details
      # return name_.to_json()
      # return [name_]
   
@app.route('/Taxi_update', methods =["GET", "POST"])
def Taxi_update():

   if request.method == "POST":


      aa=mycol.find()
      # create list of all taxis html page
      html=""
      html += "<html><body>"
      html += "<h1>Available taxis</h1>"
      html += "<table border='1'>"
      html += "<tr><th>Name</th><th>Type</th><th>X</th><th>Y</th></tr>"
      for l in aa:
            
         name=l['name']
         type=l['type']
         x = l["location"]["coordinates"][0]
         y = l["location"]["coordinates"][1]
         # create table
         html+="<tr><td>"+name+"</td><th>"+type+"</th><td>"+str(x)+"</td><td>"+str(y)+"</td></tr>"
      
      html += "</table>"
      html += "</body></html>"
      return html

if __name__ == "__main__":
    app.run(host='0.0.0.0', port=5000) 